import { render, screen } from '@testing-library/react';
import "@testing-library/jest-dom";
import React from 'react';
import App from '../src/App';

test('renders hello world', () => {
  render(<App />);
  const helloElement = screen.getByText(/hello world/i);
  expect(helloElement).toBeInTheDocument();
});
